from selenium import webdriver
from time import sleep

def toFacebook():
    usr=input('Enter Email Id:') 
    pwd=input('Enter Password:') 
    
    options = webdriver.ChromeOptions() 
    options.add_experimental_option("excludeSwitches", ["enable-logging"])
    driver = webdriver.Chrome(options=options, executable_path=r'chromedriver90')
    driver.get('https://www.facebook.com/')
    print ("Opened facebook")
    sleep(1)
    
    username_box = driver.find_element_by_id('email')
    username_box.send_keys(usr)
    print ("Email Id entered")
    sleep(1)
    
    password_box = driver.find_element_by_id('pass')
    password_box.send_keys(pwd)
    print ("Password entered")
    
    login_box = driver.find_element_by_tag_name('button')
    login_box.click()
    sleep(3)

    return driver
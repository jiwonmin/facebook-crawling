from selenium import webdriver
from time import sleep
import unicodedata
import pandas
import urllib.request
import string
import random
import login

# options = webdriver.ChromeOptions() 
# options.add_experimental_option("excludeSwitches", ["enable-logging"])
# driver = webdriver.Chrome(options=options, executable_path=r'chromedriver90')
driver = login.toFacebook()
driver.get('https://www.facebook.com/ads/library/?active_status=all&ad_type=all&country=KR&q=NIKE')
sleep(3)

driver.execute_script("window.scrollTo(0, document.body.scrollHeight)") 
sleep(3)
driver.execute_script("window.scrollTo(0, document.body.scrollHeight)") 
sleep(3)
driver.execute_script("window.scrollTo(0, document.body.scrollHeight)") 
sleep(3)

allElement = driver.find_elements_by_css_selector("div._9cb_ > div._99s5 > div._9b9p._99s6")

for_csv = []
for item in allElement:
    mediaType = ' '
    imageSrc = ' '
    imageNum = 0
    channels = []
    randomString = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(20))

    # 광고 미디어 타입 확인
    if len(item.find_elements_by_css_selector("._8mmx._8mmy")) == 0:
        if len(item.find_elements_by_css_selector("._8o0a._8o0b > video")) > 0:
            mediaType = 'video'
            imageSrc = item.find_element_by_css_selector("._8o0a._8o0b > video").get_attribute("poster")
        elif len(item.find_elements_by_css_selector(".dweymo5e > img._7jys.img")) > 0:
            mediaType = 'image'
            imageSrc = item.find_element_by_css_selector(".dweymo5e > img._7jys.img").get_attribute("src")

            # 이미지 다운로드
            # try:
            #     urllib.request.urlretrieve(imageSrc, 'images/' + randomString + '.jpg')
            # except Exception as e:
            #     print(e)
            #     continue
            if len(item.find_elements_by_css_selector("._2zgz")) >= 1:
                imageNum = len(item.find_elements_by_css_selector("._2zgz"))
            else:
                imageNum = 1

    # 광고 게재 채널
    for channel in item.find_elements_by_css_selector('.jwy3ehce'):
        maskPosition = channel.value_of_css_property('-webkit-mask-position')
        if (maskPosition == '0px -727px'): 
            channels.append('Facebook')
        elif (maskPosition == '0px -305px'): 
            channels.append('Instagram')
        elif (maskPosition == '0px -71px'): 
            channels.append('Audience Network')
        elif (maskPosition == '0px -322px'): 
            channels.append('Facebook Messange')

    for_csv.append({
        "is_active" : item.find_element_by_css_selector("div._9cd2").text,
        "posted_at" : item.find_element_by_css_selector("div._9cd3 > span.l61y9joe.jdeypxg0.and5a8ls.te7ihjl9.svz86pwt.ippphs35.a53abz89").text.replace("에 게재 시작함", ""),
        "channel" : ', '.join(channels),
        # "id" : "=\"" + item.find_element_by_css_selector("span.l61y9joe.jdeypxg0.and5a8ls.te7ihjl9.svz86pwt.jrvjs1jy.a53abz89").text.replace("ID: ", "") + "\"",
        "id" : item.find_element_by_css_selector("span.l61y9joe.jdeypxg0.and5a8ls.te7ihjl9.svz86pwt.jrvjs1jy.a53abz89").text.replace("ID: ", ""),
        "page_name" : unicodedata.normalize("NFC", item.find_element_by_css_selector("span.l61y9joe.j8otv06s.a1itoznt.te7ihjl9.svz86pwt.cu1gti5y.a53abz89").text),
        "media_type" : mediaType,
        "image" : imageSrc,
        "image_num" : imageNum,
        # " " : " "
    })

data = pandas.DataFrame(for_csv)

# print(data)
# data.to_csv('facebook_ads_library.csv', index = False, encoding = 'utf-8-sig')

def path_to_image_html(path):
    return '<img src="'+ path + '" width="120" alt="image not vaild">'

data.to_html('facebook_ads_library.html', escape=False, formatters=dict(image=path_to_image_html))

driver.quit()